# hovercarts
hovercarts is developed using Godot 4

## Goals
This project was inspired by the hovercraft racing minigame in crash bash. The motivating concept is a team/multiplayer racing game where one of the teammates is racing and the other is attempting to stifle the racer on the opoosing team.

## Project status and contributing
Currently only the mechanics have been built, but I am interested in experimentation with the hovercrafts properties acting as parameters of a synthesizer. Simultaneously the craft could draw or paint on the ground in some way.

This is an experimental project, in the ideation phase. If you want to build on this base, feel free to either fork this repository or create an issue for a feature request.

## Learning new things
I followed the parts of this tutorial related to setting up the HUD and checkpoint system: https://electronstudio.github.io/godot_racing/tutorial.html
Notably, this tutorial is for a 2D game, but the process was basically the same! The most important feature that I added to the checkpoint system was a check on the velocity direction so that laps are not added for a player that is moving backwards.
