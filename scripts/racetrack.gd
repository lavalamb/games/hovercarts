extends Node3D
var time = 0
var race_started = false
var player_dicts = [
	{
		"id": "1",
		"body": {
			"spawn_position": Vector3(-10, 10, 0)
		},
		"display": {
			"anchor_presets": Control.PRESET_TOP_LEFT,
			"layout_dir": Control.LAYOUT_DIRECTION_LTR,
			"text_info_pos_y_mod": 0,
			"position_y": 0,
		}
	},
	{
		"id": "2",
		"body": {
			"spawn_position": Vector3(-5, 10, 0)
		},
		"display": {
			"anchor_presets": Control.PRESET_TOP_LEFT,
			"layout_dir": Control.LAYOUT_DIRECTION_RTL,
			"text_info_pos_y_mod": 0,
			"position_y": 0,
		}
	},
	{
		"id": "3",
		"body": {
			"spawn_position": Vector3(5, 10, 0)
		},
		"display": {
			"anchor_presets": Control.PRESET_BOTTOM_LEFT,
			"layout_dir": Control.LAYOUT_DIRECTION_LTR,
			"text_info_pos_y_mod": -190,
			"position_y": 590,
		},
	},
	{
		"id": "4",
		"body": {
			"spawn_position": Vector3(-15, 10, 0)
		},
		"display": {
			"anchor_presets": Control.PRESET_BOTTOM_LEFT,
			"layout_dir": Control.LAYOUT_DIRECTION_RTL,
			"text_info_pos_y_mod": -190,
			"position_y": 590,
		},
	}	
]
var players_loaded = 0 # And this should start at 0
var player_scene = preload("res://scenes/player.tscn")
var boost_scene = preload("res://scenes/boost_power.tscn")
var player_details_scene = preload("res://scenes/player_details.tscn")
var input_devices = Input.get_connected_joypads()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var powerup_timer = get_node("PowerupTimer")
	powerup_timer.timeout.connect(_on_power_timer_timeout)
	
	# These lines before the loop just add the keyboard controlled player to the game
	var keyboard = -1
	var new_player = player_scene.instantiate().setup(keyboard, player_dicts[players_loaded])
	add_child(new_player)
	players_loaded += 1
	for device in input_devices:
		# Performing this check as get_connected_joypads should not include touchpads!
		# Awaiting fix: https://github.com/godotengine/godot/issues/59250
		if not Input.get_joy_name(device).contains("Touchpad"):
			new_player = player_scene.instantiate().setup(device, player_dicts[players_loaded])
			add_child(new_player)
			players_loaded += 1

func _process(delta):
	if race_started:
		time += delta
		$HUD/Control/time.text = "TIME: " + str(time).pad_zeros(3).left(6)

func _on_checkpoint_body_entered(body):
	if (body.name as String).contains("Player"):
		if body.velocity.z > 0 and body.max_progress == body.progress:
			body.progress += 1

			# Interpolate player id to get node name
			var format_string = "player_%s_details"
			var details_node_name = format_string % body.id

			var details_node = $HUD.get_node(details_node_name).get_node("text_info")
			details_node.get_node("laps").text = "LAPS: " + str(body.progress)
			body.last_lap_time = time - body.time_last_lap_completed
			details_node.get_node("last_lap").text = "LAST LAP: " + str(body.last_lap_time).pad_zeros(3).left(6)
			if body.last_lap_time < body.best_time:
				body.best_time = body.last_lap_time
				details_node.get_node("best").text = "BEST: " + str(body.best_time).pad_zeros(3).left(6)
			body.time_last_lap_completed = time
			body.max_progress = body.progress
		elif body.velocity.z > 0 and body.progress < body.max_progress:
			body.progress += 1

func _on_timer_timeout() -> void:
	race_started = true

func _on_power_timer_timeout() -> void:
	var random = RandomNumberGenerator.new()
	random.randomize()
	var num_boosts = 0
	
	## Create 30 boost powerups
	while num_boosts < 30:
		var boost_powerup = boost_scene.instantiate()
		var x = random.randf_range(-15, 15)
		var z = random.randf_range(-15, 15)
		boost_powerup.position  = Vector3(x, 0 , z)
		add_child(boost_powerup)
		num_boosts += 1

func _on_checkpoint_body_exited(body: Node3D) -> void:
	if body.velocity.z < 0:
		body.progress -= 1
