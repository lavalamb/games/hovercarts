extends Control

@export var mainGameScene: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# grab focus of the play button	
	$VBoxContainer/play_button.grab_focus()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	# As noted in https://github.com/matjlars/godot-multiplayer-input
	# The normal Input functions do not work with this add-on
	# This seems to also break the built-in Menu UI handling
	# Handling these inputs manually is required
	if MultiplayerInput.is_action_just_pressed(0, "ui_accept"):
		if $VBoxContainer/play_button.has_focus():
			$VBoxContainer/play_button.emit_signal("button_up")


func _on_button_button_up() -> void:
	get_tree().change_scene_to_packed(mainGameScene)
