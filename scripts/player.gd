extends CharacterBody3D

const GLOBAL_DIRECTION = Vector3(0,0,1)
const JUMP_VELOCITY = 4.5
const ROTATIONAL_VELOCITY_MAGNITUDE = 0.1
const Y_AXIS = Vector3(0, 1, 0)

var alternate_player_colors = ['#f5ef46', '#94f7fa', '#fa3f3c', '#b9ef73']
var respawn_position = self.position
var player_direction = GLOBAL_DIRECTION.rotated(Vector3(0,1,0), rotation.y)
var rotational_direction = 0
var thrust_magnitude = 0
var boost_thrust = 0.8
var boost_decay = 1
var is_boosting = false

# public vars
var boost_meter
var progress = 0
var max_progress = 0
var best_time = 999
var time_last_lap_completed = 0
var last_lap_time = 0

## Instance specific vars (constants)
var joy_index = 0
var player_details
var player_details_layout
var id

## Audio generator variables
var playback: AudioStreamPlayback = null # Actual playback stream, assigned in _ready().
var sample_hz = 22050.0 # Keep the number of samples to mix low, GDScript is not super fast.
var pulse_hz = 440
var phase = 0.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	var timer = get_parent_node_3d().get_node("Timer")
	timer.timeout.connect(_on_timer_timeout)
	
	## Set mix rate
	$AudioPlayer.stream.mix_rate = sample_hz # Setting mix rate is only possible before play().


func setup(new_player_index: int, player_dict: Dictionary) -> CharacterBody3D:
	joy_index = new_player_index
	id = player_dict.id
	player_details_layout = player_dict.display
	position = player_dict.body.spawn_position
	var new_material := StandardMaterial3D.new()
	new_material.albedo_color = alternate_player_colors[joy_index]
	$MeshInstance3D.material_override = new_material
	return self

func _enter_tree() -> void:
	var racetrack = get_parent_node_3d()
	player_details = racetrack.player_details_scene.instantiate()
	var format_string = "player_%s_details"
	player_details.name = format_string % id
	racetrack.get_node("HUD").add_child(player_details)
	boost_meter = player_details.get_node("boost_bar")
	(player_details as Control).set_anchors_preset(player_details_layout.anchor_presets)
	(player_details as Control).layout_direction = player_details_layout.layout_dir
	player_details.position.y += player_details_layout.position_y
	(player_details.get_node("text_info") as VBoxContainer).position.y += player_details_layout.text_info_pos_y_mod

func _physics_process(delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	else:
		respawn_position = self.position + Vector3(0, 0.2, 0)

	player_direction = GLOBAL_DIRECTION.rotated(Y_AXIS, rotation.y)

	# Handle Jump
	if MultiplayerInput.is_action_pressed(joy_index, "jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Determine rotational direction
	if MultiplayerInput.is_action_pressed(joy_index, "rotate_left"):
		rotational_direction = 1
	elif MultiplayerInput.is_action_pressed(joy_index, "rotate_right"):
		rotational_direction = -1
	else:
		rotational_direction = 0

	# Rotate player object
	rotate_object_local(Y_AXIS, rotational_direction * ROTATIONAL_VELOCITY_MAGNITUDE)

	# Handle acceleration
	if boost_meter.value > 0 and MultiplayerInput.is_action_pressed(joy_index, "boost") and is_on_floor():
		velocity += player_direction * boost_thrust
		boost_meter.value -= boost_decay
		is_boosting = true
	elif MultiplayerInput.is_action_pressed(joy_index, "accelerate") and is_on_floor():
		velocity += player_direction * thrust_magnitude
		is_boosting = false
	if self.position.y < -35:
		position = respawn_position * 0.7
		velocity = Vector3(0, 0, 0)

	move_and_slide()

func _on_timer_timeout() -> void:
	thrust_magnitude = 0.5

func _on_bumper_body_entered(body: Node3D) -> void:
	if ((body.name as String).contains("Player")):
		if body.is_boosting and not is_boosting and abs(velocity) < abs(body.velocity):
			velocity += body.velocity
			velocity.y = 10


func _fill_buffer():
	var increment = pulse_hz / sample_hz
	var to_fill = playback.get_frames_available()
	while to_fill > 0:
		playback.push_frame(Vector2.ONE * sin(phase * TAU)) # Audio frames are stereo.
		phase = fmod(phase + increment, 1) * 1.01
		to_fill -= 1
		
func _process(_delta):
	if !$AudioPlayer.is_playing() and MultiplayerInput.is_action_just_pressed(joy_index, "accelerate"):
		$AudioPlayer.play()
		playback = $AudioPlayer.get_stream_playback()
		
	if MultiplayerInput.is_action_pressed(joy_index, "accelerate"):
		$AudioPlayer.pitch_scale = velocity.length() * 0.034
		_fill_buffer()
	else:
		$AudioPlayer.stop()

	# else:
		# if $AudioPlayer.stream_paused == false:
			# $AudioPlayer.stream_paused = true
		# else:
			# $AudioPlayer.stream_paused = false

	# if Input.is_action_pressed("ui_up") and $Player.pitch_scale < 5:
		# $AudioPlayer.pitch_scale += .02
	# if Input.is_action_pressed("ui_down") and $Player.pitch_scale > .1:
		# $AudioPlayer.pitch_scale -= .02


