extends Node3D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if position.y < -5:
		var mesh = $MeshInstance3D.get_mesh()
		var timer = mesh.get_text().to_int()
		if timer > 1:
			position = Vector3(0, 10, 0)
			self.linear_velocity = Vector3(0, 0, 0)
			mesh.set_text(str(timer - 1))
		elif timer == 1:
			position = Vector3(0, 10, 0)
			self.linear_velocity = Vector3(0, 0, 0)
			mesh.set_text("GO!!!")
		else:
			self.queue_free()
